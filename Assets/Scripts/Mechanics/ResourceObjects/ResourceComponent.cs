using Atomic;

namespace SampleProject
{
    public interface IResourceComponent
    {
        AtomicVariable<ResourceType> Type { get; }
        AtomicVariable<int> Amount { get; }
    }

    public sealed class ResourceComponent : IResourceComponent
    {
        public AtomicVariable<ResourceType> Type
        {
            get { return this.type; }
        }

        public AtomicVariable<int> Amount
        {
            get { return this.amount; }
        }

        public readonly AtomicVariable<int> amount;
        public readonly AtomicVariable<ResourceType> type;

        public ResourceComponent(AtomicVariable<int> amount, AtomicVariable<ResourceType> type)
        {
            this.amount = amount;
            this.type = type;
        }
    }
}