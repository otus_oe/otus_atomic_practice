using UnityEngine;

namespace Mechanics.Position
{
    public interface ITransformComponent
    {
        Vector3 Position { get; }
        Quaternion Rotation { get; }
    }

    public sealed class TransformComponent : ITransformComponent
    {
        public Vector3 Position { get; }
        public Quaternion Rotation { get; }

        private readonly Transform transform;
        
        public TransformComponent(Transform transform)
        {
            this.transform = transform;
        }
    }
}