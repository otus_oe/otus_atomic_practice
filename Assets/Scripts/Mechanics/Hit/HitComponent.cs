using Atomic;

namespace Mechanics.Hit
{
    public interface IHitComponent
    {
        void Hit();
    }

    public sealed class HitComponent : IHitComponent
    {
        private readonly IAtomicAction onHit;

        public HitComponent(IAtomicAction onHit)
        {
            this.onHit = onHit;
        }

        public void Hit()
        {
            this.onHit.Invoke();
        }
    }
}