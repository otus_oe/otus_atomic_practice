using System;
using Atomic;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class MoveInDirectionState : FixedUpdateState
    {
        private IAtomicVariable<Vector3> _movementDirection;
        private MoveInDirectionEngine _moveInDirectionEngine;
        private RotateInDirectionEngine _rotateInDirectionEngine;

        public void Construct(
            IAtomicVariable<Vector3> movementDirection,
            MoveInDirectionEngine moveInDirectionEngine,
            RotateInDirectionEngine rotateInDirectionEngine
        )
        {
            _movementDirection = movementDirection;
            _moveInDirectionEngine = moveInDirectionEngine;
            _rotateInDirectionEngine = rotateInDirectionEngine;
        }

        protected override void FixedUpdate(float deltaTime)
        {
            this.SetDirection(_movementDirection.Value);
        }

        private void SetDirection(Vector3 direction)
        {
            _moveInDirectionEngine.SetDirection(direction);
            _rotateInDirectionEngine.SetDirection(direction);
        }
    }
}