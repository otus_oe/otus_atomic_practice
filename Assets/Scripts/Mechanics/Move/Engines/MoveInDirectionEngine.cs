using System;
using Atomic;
using Declarative;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class MoveInDirectionEngine : IUpdateListener
    {
        private Transform _transform;
        private IAtomicValue<float> _speed;

        private Vector3 _direction;
        
        public void Construct(Transform transform, AtomicVariable<float> speed)
        {
            _transform = transform;
            _speed = speed;
        }
        
        void IUpdateListener.Update(float deltaTime)
        {
            _transform.position += _direction * (_speed.Value * deltaTime);
        }

        public void SetDirection(Vector3 direction)
        {
            _direction = direction;
        }
    }
}