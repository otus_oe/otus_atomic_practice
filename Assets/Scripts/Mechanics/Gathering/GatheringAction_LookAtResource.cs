using Atomic;
using UnityEngine;

namespace SampleProject
{
    public class GatheringAction_LookAtResource : IAtomicAction<GatherResourceCommand>
    {
        private readonly Transform transform;
        
        public void Invoke(GatherResourceCommand args)
        {
            var myPosition = this.transform.position;
            var resourcePosition = args.Position;
            var direction = (resourcePosition - myPosition).normalized;
            this.transform.rotation = Quaternion.LookRotation(direction);
        }
    }
}