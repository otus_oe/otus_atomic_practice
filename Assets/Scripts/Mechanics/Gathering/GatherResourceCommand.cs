using System;
using Entities;
using Mechanics.Position;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class GatherResourceCommand
    {
        public ResourceType Type
        {
            get { return this.resourceComponent.Type.Value; }
        }

        public int Amount
        {
            get { return this.resourceComponent.Amount.Value; }
        }

        public Vector3 Position
        {
            get { return this.transformComponent.Position; }
        }

        public IEntity Resource
        {
            get { return this.resource; }
        }
        
        private readonly IEntity resource;
        private readonly ITransformComponent transformComponent;
        private readonly IResourceComponent resourceComponent;

        private bool isCompleted;

        public GatherResourceCommand(IEntity resource)
        {
            this.resource = resource;

            this.resourceComponent = resource.Get<IResourceComponent>();
            this.transformComponent = resource.Get<ITransformComponent>();
        }

        public void Complete()
        {
            this.isCompleted = true;
        }

        public bool IsCompleted()
        {
            return this.isCompleted;
        }
    }
}