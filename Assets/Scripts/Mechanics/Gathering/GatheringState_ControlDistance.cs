using System;
using Atomic;
using SampleProject;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class GatheringState_ControlDistance : FixedUpdateState
    {
        private Transform myTransform;
        private AtomicProcess<GatherResourceCommand> process;
        private AtomicVariable<float> minDistance;

        public void Construct(
            Transform myTransform,
            AtomicProcess<GatherResourceCommand> process,
            AtomicVariable<float> minDistance
        )
        {
            this.myTransform = myTransform;
            this.process = process;
            this.minDistance = minDistance;
        }
        
        protected override void FixedUpdate(float deltaTime)
        {
            if (Vector3.Distance(this.myTransform.position, process.State.Position) > this.minDistance.Value)
            {
                process.Stop();
            }
        }
    }
}