using System;
using Atomic;

namespace SampleProject
{
    public interface IGatherResourceComponent
    {
        event Action<GatherResourceCommand> OnStopped;

        void Start(GatherResourceCommand command);

        void Stop();
    }

    public sealed class GatherResourceComponent : IGatherResourceComponent
    {
        public event Action<GatherResourceCommand> OnStopped
        {
            add { this.process.OnStopped += value; }
            remove { this.process.OnStopped -= value; }
        }

        private readonly IAtomicProcess<GatherResourceCommand> process;

        public GatherResourceComponent(IAtomicProcess<GatherResourceCommand> process)
        {
            this.process = process;
        }

        public void Start(GatherResourceCommand command)
        {
            this.process.Start(command);
        }

        public void Stop()
        {
            this.process.Stop();
        }
    }
}