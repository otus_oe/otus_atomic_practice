using System;
using Atomic;
using SampleProject;

namespace SampleProject
{
    [Serializable]
    public sealed class GatheringState_TimeProgress : UpdateState
    {
        private float currentTime;

        private AtomicVariable<float> duration;
        private IAtomicProcess<GatherResourceCommand> process;

        public void Construct(
            AtomicVariable<float> duration,
            IAtomicProcess<GatherResourceCommand> process
        )
        {
            this.duration = duration;
            this.process = process;
        }

        protected override void Enter()
        {
            this.currentTime = 0.0f;
        }

        protected override void Update(float deltaTime)
        {
            this.currentTime += deltaTime;
            if (this.currentTime >= this.duration.Value)
            {
                this.process.State.Complete();
                this.process.Stop();
            }
        }
    }
}