using Atomic;
using UnityEngine;

namespace SampleProject
{
    public class GatheringCondition_CheckDistance : IAtomicFunction<GatherResourceCommand, bool>
    {
        private readonly Transform transform;
        private readonly IAtomicValue<float> minDistance;
        
        public GatheringCondition_CheckDistance(Transform transform, IAtomicValue<float> minDistance)
        {
            this.transform = transform;
            this.minDistance = minDistance;
        }

        public bool Invoke(GatherResourceCommand args)
        {
            var myPosition = this.transform.position;
            var resourcePosition = args.Position;
            return Vector3.Distance(myPosition, resourcePosition) <= this.minDistance.Value;

        }
    }
}