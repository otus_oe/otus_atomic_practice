using System;
using Atomic;
using UnityEngine;

namespace SampleProject
{
    public interface ICollisionComponent
    {
        event Action<Collision> OnEntered;
        event Action<Collision> OnExited;
    }

    public sealed class CollisionComponent : ICollisionComponent
    {
        public event Action<Collision> OnEntered
        {
            add { sensor.OnEntered += value; }
            remove { sensor.OnEntered -= value; }
        }

        public event Action<Collision> OnExited
        {
            add { sensor.OnExited += value; }
            remove { sensor.OnExited -= value; }
        }

        private readonly CollisionObservable sensor;

        public CollisionComponent(CollisionObservable sensor)
        {
            this.sensor = sensor;
        }
    }
}