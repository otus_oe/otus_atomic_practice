using Entities;
using GameSystem;
using System;
using UnityEngine;

namespace SampleProject.Controllers
{
    public sealed class CharacterGatheringController : MonoBehaviour,
        IGameInitElement,
        IGameFinishElement
    {
        //CharacterResourceVisitController
        //2nd controller - current resource -> onAmountChanged + reourceStorage
        //3rd controller - stop gathering

        [SerializeField]
        private MonoEntity character;

        private ICollisionComponent _collisionComponent;

        private IGatherResourceComponent _resourceComponent;

        void IGameInitElement.InitGame()
        {
            _collisionComponent = character.Get<CollisionComponent>();
            _collisionComponent.OnEntered += TryGatherResource;
            _collisionComponent.OnExited += StopGatherResource;
            _resourceComponent = character.Get<IGatherResourceComponent>();
        }

        private void TryGatherResource(Collision collision)
        {
            Debug.Log("in TryGatherResource");
            var resource = collision.gameObject.GetComponent<IEntity>();
            if (resource != null)
            {
                return;
            }
            IResourceComponent resourceComponent;
            if(resource.TryGet<IResourceComponent>(out resourceComponent) && resourceComponent.Amount.Value > 0)
            {
                Debug.Log($"{resourceComponent.Type}: {resourceComponent.Amount}");
                _resourceComponent.Start(new GatherResourceCommand(resource));
                resourceComponent.Amount.OnChanged += OnAmountChanged;
            }
        }

        private void OnAmountChanged(int amount)
        {
            if (amount <= 0)
            {
                _resourceComponent.Stop();
            }
        }

        private void StopGatherResource(Collision collision)
        {
            Debug.Log("in StopGatherResource");
            var proxy = collision.gameObject.GetComponent<MonoEntityProxy>();
            if (!proxy)
            {
                return;
            }
            var entity = proxy.entity;
            IResourceComponent resource;
            if (entity.TryGet<IResourceComponent>(out resource))
            {
                resource.Amount.OnChanged -= OnAmountChanged;
                _resourceComponent.Stop();
            }
        }

        void IGameFinishElement.FinishGame()
        {
            _collisionComponent.OnEntered -= TryGatherResource;
            _collisionComponent.OnExited -= StopGatherResource;
        }
    }
}
