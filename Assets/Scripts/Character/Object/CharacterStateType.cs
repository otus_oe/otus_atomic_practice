namespace SampleProject
{
    public enum CharacterStateType
    {
        Idle,
        Run,
        Dead,
        Gathering
    }
}