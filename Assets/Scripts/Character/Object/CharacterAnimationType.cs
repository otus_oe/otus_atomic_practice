namespace SampleProject
{
    public enum CharacterAnimationType
    {
        Idle = 0,
        Run = 1,
        Chop = 2,
        Mine = 4,
        Dead = 5
    }
}