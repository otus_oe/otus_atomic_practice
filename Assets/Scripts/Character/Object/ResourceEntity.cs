using Entities;
using Mechanics.Hit;
using Mechanics.Position;
using UnityEngine;

namespace SampleProject
{
    [DefaultExecutionOrder(-100)]
    public sealed class ResourceEntity : MonoEntityBase
    {
        private void Awake()
        {
            var resourceModel = this.GetComponent<ResourceModel>();
            Add(new ResourceComponent(resourceModel.core.amount, resourceModel.core.type));
            Add(new TransformComponent(resourceModel.core.transform));
            Add(new HitComponent(resourceModel.core.onHit));
        }
    }
}