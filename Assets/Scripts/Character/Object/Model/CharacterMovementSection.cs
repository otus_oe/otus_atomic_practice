using System;
using Atomic;
using Declarative;
using SampleProject;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class CharacterMovementSection
    {
        public AtomicVariable<float> movementSpeed = new(6f);
        public AtomicVariable<float> rotationSpeed = new(10f);
        public AtomicVariable<Vector3> movementDirection;

        public MoveInDirectionEngine moveInDirectionEngine;
        public RotateInDirectionEngine rotateInDirectionEngine;

        [Section]
        public State state;

        [Construct]
        public void Construct(CharacterModel.Core core)
        {
            moveInDirectionEngine.Construct(core.transform, movementSpeed);
            rotateInDirectionEngine.Construct(core.transform, rotationSpeed);
        }

        [Serializable]
        public sealed class State : CompositeState
        {
            public MoveInDirectionState moveState;

            [Construct]
            public void ConstructSelf()
            {
                SetStates(moveState);
            }

            [Construct]
            public void ConstructSubStates(CharacterVisualSection visual, CharacterMovementSection movementSection)
            {
                moveState.Construct(
                    movementSection.movementDirection,
                    movementSection.moveInDirectionEngine,
                    movementSection.rotateInDirectionEngine
                );
            }
        }
    }
}