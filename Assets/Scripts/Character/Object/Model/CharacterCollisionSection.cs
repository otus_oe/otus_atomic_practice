using System;
using Atomic;

namespace SampleProject
{
    [Serializable]
    public sealed class CharacterCollisionSection
    {
        public CollisionObservable sensor;
        public TransformSynchronizer synchronizer;
    }
}