using System;
using Atomic;
using Declarative;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class CharacterStatesSection
    {
        public StateMachine<CharacterStateType> stateMachine;

        [Construct]
        public void Construct(
            CharacterModel root,
            CharacterMovementSection movement,
            CharacterGatheringSection gathering
        )
        {
            root.onStart += () => this.stateMachine.Enter();

            stateMachine.SetupStates(
                (CharacterStateType.Idle, null),
                (CharacterStateType.Run, movement.state),
                (CharacterStateType.Dead, null),
                (CharacterStateType.Gathering, null)
            );
        }

        [Construct]
        public void ConstructTransitions(
            CharacterModel root,
            CharacterLifeSection life,
            CharacterMovementSection movement,
            CharacterGatheringSection gathering
        )
        {
            life.isAlive.OnChanged += isAlive =>
            {
                var stateType = isAlive ? CharacterStateType.Idle : CharacterStateType.Dead;
                stateMachine.SwitchState(stateType);
            };

            gathering.process.OnStarted += _ =>
            {
                if (life.isAlive.Value)
                {
                    stateMachine.SwitchState(CharacterStateType.Gathering);
                }
            };

            gathering.process.OnStopped += _ =>
            {
                if (life.isAlive.Value)
                {
                    stateMachine.SwitchState(CharacterStateType.Idle);
                }
            };
            
            root.onFixedUpdate += _ =>
            {
                if (!life.isAlive.Value)
                {
                    return;
                }

                if (movement.movementDirection.Value != Vector3.zero && stateMachine.CurrentState == CharacterStateType.Idle)
                {
                    stateMachine.SwitchState(CharacterStateType.Run);
                }
                else if (movement.movementDirection.Value == Vector3.zero && 
                         stateMachine.CurrentState == CharacterStateType.Run)
                {
                    stateMachine.SwitchState(CharacterStateType.Idle);
                }
            };
        }
    }
}