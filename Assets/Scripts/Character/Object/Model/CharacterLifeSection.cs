using System;
using Atomic;

namespace SampleProject
{
    [Serializable]
    public sealed class CharacterLifeSection
    {
        public AtomicVariable<bool> isAlive;
    }
}