using System;
using Atomic;
using SampleProject;
using Declarative;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class CharacterGatheringSection
    {
        public AtomicVariable<float> duration = new(3);
        public AtomicVariable<float> minDistance = new(1.25f);

        public AtomicProcess<GatherResourceCommand> process;

        [Construct]
        public void Construct(CharacterModel root, CharacterMovementSection movement)
        {
        }
    }
}