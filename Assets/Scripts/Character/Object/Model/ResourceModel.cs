using System;
using SampleProject;
using Declarative;
using UnityEngine;
using Atomic;

namespace SampleProject
{
    public class ResourceModel : DeclarativeModel
    {
        [Section]
        public Core core;

        //[Section]
        //public ResourceVisualSection visual;

        [Serializable]
        public sealed class Core
        {
            public Transform transform;

            public AtomicVariable<int> amount;
            public AtomicVariable<ResourceType> type;
            public AtomicEvent onHit;

            [Construct]
            private void Construct()
            {
                onHit += () =>
                {
                    Debug.Log("onHit");
                    this.amount.Value--;
                    if (this.amount.Value <= 0)
                    {
                        transform.gameObject.SetActive(false);
                    }
                };
            }
        }
    }
}