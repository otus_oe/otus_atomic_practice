using System;
using SampleProject;
using Declarative;
using UnityEngine;

namespace SampleProject
{
    public sealed class CharacterModel : DeclarativeModel
    {
        [Section]
        public Core core;

        [Section]
        public CharacterVisualSection visual;
        
        [Serializable]
        public sealed class Core
        {
            public Transform transform;
        
            [Section]
            public CharacterLifeSection life;

            [Section]
            public CharacterMovementSection movement;

            [Section]
            public CharacterGatheringSection gathering;

            [Section]
            public CharacterCollisionSection collision;

            [Section]
            public CharacterStatesSection states;
        }
    }
}