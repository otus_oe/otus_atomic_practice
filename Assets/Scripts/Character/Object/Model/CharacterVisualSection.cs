using System;
using Atomic;
using Declarative;
using Mechanics.Hit;
using UnityEngine;

namespace SampleProject
{
    [Serializable]
    public sealed class CharacterVisualSection
    {
        public AnimatorStateMachine<CharacterAnimationType> animatorMachine;

        public ParticleSystem chopVFX;
        public ParticleSystem mineVFX;

        public AudioSource audioSource;
        public AudioClip chopSFX;
        public AudioClip mineSFX;

        [Construct]
        public void ConstructTransitions(CharacterStatesSection states, CharacterGatheringSection gathering)
        {
            var coreFSM = states.stateMachine;
            var process = gathering.process;

            this.animatorMachine.SetupTransitions(
                (CharacterAnimationType.Idle, () => coreFSM.CurrentState == CharacterStateType.Idle),
                (CharacterAnimationType.Run, () => coreFSM.CurrentState == CharacterStateType.Run),
                (CharacterAnimationType.Dead, () => coreFSM.CurrentState == CharacterStateType.Dead),
                (CharacterAnimationType.Chop, () => coreFSM.CurrentState == CharacterStateType.Gathering &&
                                               process.State.Type == ResourceType.WOOD),
                (CharacterAnimationType.Mine, () => coreFSM.CurrentState == CharacterStateType.Gathering &&
                                               process.State.Type == ResourceType.STONE)
            );
        }

        [Construct]
        public void Construct(CharacterGatheringSection gathering)
        {
            this.animatorMachine.OnMessageReceived += message =>
            {
                if (message == "chop")
                {
                    this.chopVFX.Play();
                    this.audioSource.PlayOneShot(this.chopSFX);

                    if (gathering.process.State != null)
                    {
                        gathering.process.State.Resource.Get<IHitComponent>().Hit();
                    }
                }
                else if (message == "mine")
                {
                    this.mineVFX.Play();
                    this.audioSource.PlayOneShot(this.mineSFX);

                    if (gathering.process.State != null)
                    {
                        gathering.process.State.Resource.Get<IHitComponent>().Hit();
                    }
                }
            };
        }
    }
}