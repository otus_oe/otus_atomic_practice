using Entities;
using GameSystem;
using UnityEngine;

namespace SampleProject.Controllers
{
    public sealed class CharacterMovementController : MonoBehaviour,
        IGameInitElement,
        IGameFinishElement
    {
        [SerializeField]
        private MovementInput _movementInput;
         
        [SerializeField]
        private MonoEntity character;

        private MoveInDirectionComponent _moveInDirection;

        void IGameInitElement.InitGame()
        {
            _moveInDirection = character.Get<MoveInDirectionComponent>();
            _movementInput.MovementDirectionChanged += OnMovementDirectionChanged;
        }

        void IGameFinishElement.FinishGame()
        {
            _movementInput.MovementDirectionChanged -= OnMovementDirectionChanged;
        }

        private void OnMovementDirectionChanged(Vector3 direction)
        {
            _moveInDirection.MoveInDirection(direction);
        }
    }
}