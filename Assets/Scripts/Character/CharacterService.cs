using Entities;
using UnityEngine;

namespace SampleProject
{
    public sealed class CharacterService : MonoBehaviour
    {
        public IEntity Character
        {
            get { return this.character; }
        }

        [SerializeField]
        private MonoEntity character;
    }
}