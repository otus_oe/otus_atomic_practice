using System.Collections.Generic;
using SampleProject;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Player
{
    public sealed class PlayerResourceStorage : MonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        private Dictionary<ResourceType, int> resources;

        public void SetResource(ResourceType resourceType, int resource)
        {
            this.resources[resourceType] = resource;
        }
        
        public int GetResource(ResourceType resourceType)
        {
            return this.resources[resourceType];
        }
    }
}