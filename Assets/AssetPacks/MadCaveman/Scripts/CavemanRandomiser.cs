﻿//Script by Andy Noworol /twitter => @andynoworol
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MadCaveman {
    public class CavemanRandomiser : MonoBehaviour {
        [Header("Randomisation")]
        public GameObject[] mainMeshes;
        public GameObject[] headOptions;
        public GameObject[] faceOptions;
        public GameObject[] accesories;
        public Material[] mainMaterials;
        public Material[] overlayMaterials;

        private void Start() {
            Randomize();
        }
        private void Randomize() {
            float rScale = Random.Range(0.9f, 1.1f);
            transform.localScale = Vector3.one * rScale;

            GetComponentInChildren<Animator>().speed = 2.0f - rScale;

            int rHead = Random.Range(-1, headOptions.Length);
            if (rHead != -1) {
                for (int i = 0; i < headOptions.Length; i++) {
                    headOptions[i].SetActive(false);
                }
                headOptions[rHead].SetActive(true);
            }

            int rFace = Random.Range(-1, faceOptions.Length);
            if (rFace != -1) {
                for (int i = 0; i < faceOptions.Length; i++) {
                    faceOptions[i].SetActive(false);
                }
                faceOptions[rFace].SetActive(true);
            }

            int rAcc = Random.Range(-1, accesories.Length);
            if (rAcc != -1) {
                for (int i = 0; i < accesories.Length; i++) {
                    accesories[i].SetActive(false);
                }
                accesories[rAcc].SetActive(true);
            }
            int rMain = Random.Range(0, mainMeshes.Length);
            for (int i = 0; i < mainMeshes.Length; i++) {
                mainMeshes[i].SetActive(false);
            }
            mainMeshes[rMain].SetActive(true);
            Renderer mainRenderer = mainMeshes[rMain].GetComponent<Renderer>();
            Material[] mats = new Material[2];

            int rMaterials = Random.Range(0, mainMaterials.Length);
            mats[0] = mainMaterials[rMaterials];

            int rOverlays = Random.Range(-1, overlayMaterials.Length);
            if (rOverlays != -1) {
                mats[1] = overlayMaterials[rOverlays];
            }

            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderers.Length; i++) {
                renderers[i].sharedMaterials = mats;
            }
        }
    }
}
