﻿//Script by Andy Noworol /twitter => @andynoworol
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MadCaveman {
    public class CavemanController : MonoBehaviour {
        [Header("Setup")]
        public bool controlled;
        public float characterSpeed = 2.5f;
        public bool club;
        public float gravity = -9.81f;
        public int idleAnimationVariations = 2;

        [Header("References")]
        public CharacterController characterController;
        public Animator anim;
        public Transform groundCheck;
        public GameObject clubMesh;

        private int animState = -1;
        private bool grounded;
        private Vector2 input;
        private Vector3 gravityVel;
        private Quaternion goalLookRotation;
        private Coroutine idleCoroutine;

        private void Start() {
            if (club) clubMesh.SetActive(true); else clubMesh.SetActive(false);
            anim.SetBool("Club", club);
        }

        private void Update() {
            if (controlled) {
                input.x = Input.GetAxisRaw("Horizontal");
                input.y = Input.GetAxisRaw("Vertical");
                input = Vector3.Normalize(input);
                transform.rotation = Quaternion.Lerp(transform.rotation, goalLookRotation, 0.05f);
            }
        }

        private void FixedUpdate() {
            characterController.Move(new Vector3(input.x, 0.0f, input.y) * Time.deltaTime * characterSpeed);
            float velocity = Vector3.Magnitude(characterController.velocity);
            if (velocity > 0.1f) {
                if (animState != 1) {
                    animState = 1;
                    anim.SetInteger("State", animState);
                    if (idleCoroutine != null) StopCoroutine(idleCoroutine);
                }
            } else {
                if (animState != 0) {
                    animState = 0;
                    anim.SetInteger("State", animState);
                    idleCoroutine = StartCoroutine(IdleLoop());
                }
            }
            grounded = GroundCheck();
            ApplyFreeFall();
            if (animState == 1) {
                goalLookRotation = Quaternion.LookRotation(new Vector3(input.x, 0.0f, input.y));
            }
        }

        
        private bool GroundCheck() {
            bool result = Physics.CheckSphere(groundCheck.position, 0.1f, LayerMask.NameToLayer("Ground"));
            return result;
        }
        private void ApplyFreeFall() {
            gravityVel.y += gravity * Time.deltaTime;
            characterController.Move(gravityVel * Time.deltaTime);

            //Velocity reset
            if (grounded && gravityVel.y < 0.0f) {
                gravityVel.y = -2.0f;
            }
        }

        private IEnumerator IdleLoop() {
            while (true) {
                float time = Random.Range(5.0f, 10.0f);
                yield return new WaitForSeconds(time);
                int rIdle = Random.Range(0, idleAnimationVariations);
                anim.SetTrigger(rIdle.ToString());
            }
        }
    }
}

