using Declarative;

namespace Atomic
{
    public abstract class UpdateState : IState, IUpdateListener
    {
        private bool enabled;

        void IState.Enter()
        {
            this.enabled = true;
            this.Enter();
        }

        void IState.Exit()
        {
            this.enabled = false;
            this.Exit();
        }

        void IUpdateListener.Update(float deltaTime)
        {
            if (this.enabled)
            {
                this.Update(deltaTime);
            }
        }

        protected virtual void Enter()
        {
        }

        protected virtual void Exit()
        {
        }

        protected abstract void Update(float deltaTime);
    }
}