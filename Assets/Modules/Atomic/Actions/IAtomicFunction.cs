namespace Atomic
{
    public interface IAtomicFunction<out R>
    {
        R Invoke();
    }

    public interface IAtomicFunction<in T, out R>
    {
        R Invoke(T args);
    }
}